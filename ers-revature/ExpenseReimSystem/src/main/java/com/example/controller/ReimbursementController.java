package com.example.controller;


import java.util.Date;
import java.util.List;

import com.example.model.ApprovalRequest;
import com.example.model.Employee;
import com.example.model.Reimbursement;
import com.example.service.ReimbursementService;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.javalin.http.Handler;

public class ReimbursementController {
	
	private ReimbursementService reimServ;
	
	public  Handler POST_REQUEST = (ctx) -> {
		Employee user = (Employee)ctx.sessionAttribute("user");
		
		Double amount = Double.parseDouble(ctx.formParam("amount"));
		String description = ctx.formParam("description");
		int author = user.getId();
		int type = Integer.parseInt(ctx.formParam("type"));
		
		reimServ.createReimbRequest(new Reimbursement(amount,null, null, description, author, 1, 1, type));
		
		if(ctx.status()==200) {
			ctx.redirect("/html/request.html");
		}
	};
	
	public  Handler GET_REIMBURSEMENTLIST = (ctx) -> {
		Employee user = (Employee)ctx.sessionAttribute("user");
		List<Reimbursement> reimbursement =  reimServ.getReimburmentList();
		ctx.status(200);
		ctx.json(reimbursement);
	};
	
	public  Handler Get_ReimbersementListByUserId = (ctx) -> {
		Employee user = (Employee)ctx.sessionAttribute("user");
		int author = user.getId();
		List<Reimbursement> reImbList = reimServ.getReimbersementListByUserId(author);	
		ctx.status(200);
		ctx.json(reImbList);
	};
	
	public  Handler Get_ReimbersementListByStatusId = (ctx) -> {
		Employee user = (Employee)ctx.sessionAttribute("user");
		int statusId = Integer.parseInt(ctx.pathParam("statusId"));
		List<Reimbursement> reImbList = reimServ.selectReimbursementByStatus(statusId);	
		ctx.status(200);
		ctx.json(reImbList);
	};
	
	public  Handler Put_updateReimbursement = (ctx) -> {
		Employee user = (Employee)ctx.sessionAttribute("user");
		int resolver = user.getId();
		String body = ctx.body();
		ObjectMapper objectMapper = new ObjectMapper();
		ApprovalRequest approvalReqeust = objectMapper.readValue(body, ApprovalRequest.class);
		int statusid = approvalReqeust.getStatus_id();
		Date approvedDate = new Date();
		int reimId = approvalReqeust.getId();
		
		reimServ.updateReimbursement(new Reimbursement( reimId, approvedDate, resolver, statusid));
		
		if(ctx.status()==200) {
			ctx.redirect("/html/request.html");
		}
	};
	
	public  Handler Delete_ReimbursementById = (ctx) -> {
		Reimbursement id = (Reimbursement) reimServ.getReimbersementListByUserId(Integer.parseInt(ctx.pathParam("reimId")));
		if(id != null) {
			reimServ.deleteReimbursementById(Integer.parseInt(ctx.pathParam("reimId")));
			 ctx.redirect("/html/request.html");
		}else {
			ctx.json("id not found");
			ctx.redirect("/html/userRequestList.html");
		}
	};
	
	public ReimbursementController() { }
		

	public ReimbursementController(ReimbursementService reimServ) {
		super();
		this.reimServ = reimServ;
	}

}
