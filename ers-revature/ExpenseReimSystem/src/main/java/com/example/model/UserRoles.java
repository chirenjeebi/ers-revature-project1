package com.example.model;

public class UserRoles {
	private int id;
	private String type;
	
	public UserRoles() {
		// TODO Auto-generated constructor stub
	}
	
	public UserRoles(String type) {
		this.type = type;
	}

	public UserRoles(int id, String type) {
		super();
		this.id = id;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "UserRoles [id=" + id + ", type=" + type + "]";
	}
	
	

}
