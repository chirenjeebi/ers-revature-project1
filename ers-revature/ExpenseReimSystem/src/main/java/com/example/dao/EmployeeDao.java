package com.example.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

import org.apache.log4j.Logger;

import com.example.connection.DBConnection;
import com.example.model.Employee;
import com.example.model.UserRoles;



public class EmployeeDao implements GenericDao<Employee> {
	private DBConnection dvcon;
	
	public EmployeeDao() {
		dvcon = new DBConnection();
	}
	
	public EmployeeDao(DBConnection dvcon) {
		super();
		this.dvcon = dvcon;
	}

	@Override
	public boolean insert(Employee employee) {
		boolean success = false;
		
		PreparedStatement prepStmt;
		try(Connection con = dvcon.getMyConnection()) {
			prepStmt = con.prepareStatement("insert into ers_users (ers_username, ers_password, user_firstName, user_lastName, user_email) values (?, ?, ?, ?, ?)");       
			prepStmt.setString(1, employee.getUsername());
			prepStmt.setString(2, employee.getPassword());
			prepStmt.setString(3, employee.getFirstName());
			prepStmt.setString(4, employee.getLastName());
			prepStmt.setString(5, employee.getEmail());
			success =  prepStmt.execute();
		} catch(SQLException e){
			e.printStackTrace();
		}
		return success;
	}

	@Override
	public boolean update(Employee employee) {
		return false;
	}

	@Override
	public Employee selectById(int empId) {
		return null;
	}
	
	@Override
	public Employee selectByUser(String username) {
		PreparedStatement prepStmt;
		try(Connection con = dvcon.getMyConnection()){
//			String sql = "SELECT e.ers_users_id, e.user_firstName, e.user_lastName, e.ers_username, e.ers_password, e.user_email, .user_role_id FROM ers_users e INNER JOIN ers_user_roles r ON e.user_role_id = r.user_role_id  WHERE e.ers_username = ?";                   
			String sql = "SELECT * FROM ers_users WHERE ers_username = ?";                   
			prepStmt= con.prepareStatement(sql);
			prepStmt.setString(1, username);
			ResultSet rs = prepStmt.executeQuery();
			
			if (!rs.first()) {
				return null;
			}
			Employee user = new Employee(rs.getInt(1), rs.getString(4), rs.getString(5),rs.getString(2),rs.getString(3),rs.getString(6),rs.getInt(7));
				return user;

		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Set<Employee> selectAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteEmployeeUser(int empId) {
		// TODO Auto-generated method stub
		return false;
	}

}
