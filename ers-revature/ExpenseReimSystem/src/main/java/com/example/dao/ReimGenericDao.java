package com.example.dao;

import java.sql.Date;
import java.util.List;
import java.util.Set;

import com.example.model.Reimbursement;

public interface ReimGenericDao<E> {
	
	//Request for new reimbursement in the to the manager
	public boolean insertRequest(Reimbursement reimburse);
	
	
	// view all new reimbursement request
	public List<Reimbursement> selectRequestAll();
	
	public List<Reimbursement> getByEmployeeId(int empId);
	
	//updating the reimbursement status by manager
	public int updateReimbursement(Reimbursement reimbursement);
	
	// deleting the Reimbursement request by reimId my manager or employee if pending status.
	public Reimbursement deleteById(int reimId);
	
	// view all request by manager selecting status of the reimbursement. 
	public List<Reimbursement> SelectByStatusType(int statusTypeId);


	


	
	
	
	
	
	
	

}
